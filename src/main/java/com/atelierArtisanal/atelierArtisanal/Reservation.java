package com.atelierArtisanal.atelierArtisanal;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;


@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "date_reservation")
    private LocalDateTime dateReservation;
    // Autres attributs
    
    @ManyToOne
    @JoinColumn(name = "participant_id")
    private Participant participant;
    
    @ManyToOne
    @JoinColumn(name = "atelier_id")
    private Atelier atelier;
    
    // Constructeurs
    public Reservation() {
    }
    
    public Reservation(LocalDateTime dateReservation) {
        this.dateReservation = dateReservation;
    }
    
    // Getters et Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(LocalDateTime dateReservation) {
        this.dateReservation = dateReservation;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public Atelier getAtelier() {
        return atelier;
    }

    public void setAtelier(Atelier atelier) {
        this.atelier = atelier;
    }

    // Autres méthodes nécessaires selon les besoins de votre application
}
