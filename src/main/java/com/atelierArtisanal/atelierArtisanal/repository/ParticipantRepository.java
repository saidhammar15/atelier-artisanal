package com.atelierArtisanal.atelierArtisanal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.atelierArtisanal.atelierArtisanal.Participant;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {
}
