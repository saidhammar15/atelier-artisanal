package com.atelierArtisanal.atelierArtisanal.repository;

import com.atelierArtisanal.atelierArtisanal.Atelier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtelierRepository extends JpaRepository<Atelier, Long> {
}