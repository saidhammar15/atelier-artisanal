package com.atelierArtisanal.atelierArtisanal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.atelierArtisanal.atelierArtisanal.Artisan;

@Repository
public interface ArtisanRepository extends JpaRepository<Artisan, Long> {
}
