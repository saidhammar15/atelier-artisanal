package com.atelierArtisanal.atelierArtisanal.controller;

import com.atelierArtisanal.atelierArtisanal.Artisan;
import com.atelierArtisanal.atelierArtisanal.service.ArtisanService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class ArtisanController {

    @Autowired
    private ArtisanService artisanService;

    // Afficher la liste des artisans
    @GetMapping("/artisans")
    public String listArtisans(Model model) {
        List<Artisan> artisans = artisanService.getAllArtisans();
        model.addAttribute("artisans", artisans);
        return "artisans/listeArtisans"; // Vue Thymeleaf correspondante
    }

    // Afficher les détails d'un artisan
    @GetMapping("/artisans/{id}")
    public String showArtisanDetails(@PathVariable("id") Long id, Model model) {
        Artisan artisan = artisanService.getArtisanById(id)
                .orElseThrow(() -> new IllegalArgumentException("Artisan non trouvé avec l'identifiant : " + id));
        model.addAttribute("artisan", artisan);
        return "artisans/detailsArtisan"; // Vue Thymeleaf correspondante
    }

    // Ajouter un nouvel artisan
    @PostMapping("/artisans")
    public String addArtisan(Artisan artisan) {
        artisanService.addArtisan(artisan);
        return "redirect:/artisans"; // Redirection vers la liste des artisans
    }

    // Mettre à jour les informations d'un artisan
    @PostMapping("/artisans/{id}")
    public String updateArtisan(@PathVariable("id") Long id, Artisan artisan) {
        artisan.setId(id);
        artisanService.updateArtisan(artisan);
        return "redirect:/artisans"; // Redirection vers la liste des artisans
    }

    // Supprimer un artisan
    @PostMapping("/artisans/{id}/delete")
    public String deleteArtisan(@PathVariable("id") Long id) {
        artisanService.deleteArtisanById(id);
        return "redirect:/artisans"; // Redirection vers la liste des artisans
    }
}
