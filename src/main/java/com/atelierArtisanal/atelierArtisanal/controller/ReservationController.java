package com.atelierArtisanal.atelierArtisanal.controller;

import com.atelierArtisanal.atelierArtisanal.Reservation;
import com.atelierArtisanal.atelierArtisanal.service.ReservationService;
import com.atelierArtisanal.atelierArtisanal.Participant;
import com.atelierArtisanal.atelierArtisanal.Atelier;
import com.atelierArtisanal.atelierArtisanal.service.ParticipantService;
import com.atelierArtisanal.atelierArtisanal.service.AtelierService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class ReservationController {

    @Autowired
    private ReservationService reservationService;
    
    @Autowired
    private ParticipantService participantService;

    @Autowired
    private AtelierService atelierService;

    @GetMapping("/reservations")
    public String listReservations(Model model) {
        List<Reservation> reservations = reservationService.getAllReservations();
        model.addAttribute("reservations", reservations);
        
        // Chargez les détails du participant pour chaque réservation et ajoutez-les au modèle
        for (Reservation reservation : reservations) {
            Participant participant = reservation.getParticipant();
            if (participant != null) {
                Long participantId = participant.getId(); // Obtenez l'ID du participant à partir de la réservation
                model.addAttribute("participantId", participantId);
            } else {
                // Gérer le cas où le participant est null
                // Vous pouvez choisir de ne rien faire ou d'ajouter un ID de participant nul au modèle
                model.addAttribute("participantId", null);
            }
        }

        return "reservations/listeReservations";
    }



    // Afficher les détails d'une réservation
    @GetMapping("/reservations/{id}")
    public String showReservationDetails(@PathVariable("id") Long id, Model model) {
        Reservation reservation = reservationService.getReservationById(id)
                .orElseThrow(() -> new IllegalArgumentException("Réservation non trouvée avec l'identifiant : " + id));
        model.addAttribute("reservation", reservation);
        return "reservations/detailsReservation"; // Vue Thymeleaf correspondante
    }
    
    @GetMapping("/reserver/{id}")
    public String showReservationPage(@PathVariable Long id, Model model) {
        // Vous pouvez utiliser l'ID pour récupérer les détails de l'atelier si nécessaire
        Optional<Atelier> atelierOptional = atelierService.getAtelierById(id);
        if (atelierOptional.isPresent()) {
            Atelier atelier = atelierOptional.get();
            model.addAttribute("atelier", atelier);
        } else {
            // Gérer le cas où l'atelier n'est pas trouvé
            // Vous pouvez rediriger vers une page d'erreur ou effectuer une autre action appropriée
        }

        // Ajoutez d'autres données nécessaires à la page de réservation
        List<Participant> participants = participantService.getAllParticipants();
        model.addAttribute("participants", participants);

        // Retournez la vue de réservation
        return "ateliers/reservation";
    }

    // Ajouter une nouvelle réservation VERSION 2
    @PostMapping("/reservations")
    public String addReservation(@ModelAttribute("reservation") Reservation reservation, @RequestParam("atelierId") Long atelierId, @RequestParam("participantId") Long participantId, Model model) {
        // Validation des données de réservation
        if (reservation.getDateReservation() == null || participantId == null) {
            // Les données de réservation sont incomplètes, renvoyer vers la page de réservation avec un message d'erreur
            model.addAttribute("error", "Veuillez remplir tous les champs.");
            List<Participant> participants = participantService.getAllParticipants();
            model.addAttribute("participants", participants);
            return "ateliers/reservation";
        }

        try {
            // Récupérer l'atelier à partir de son ID
            Optional<Atelier> atelierOptional = atelierService.getAtelierById(atelierId);
            if (!atelierOptional.isPresent()) {
                throw new IllegalArgumentException("L'atelier avec l'ID spécifié n'existe pas.");
            }

            Atelier atelier = atelierOptional.get();

            // Récupérer le participant à partir de son ID
            Optional<Participant> participantOptional = participantService.getParticipantById(participantId);
            if (!participantOptional.isPresent()) {
                throw new IllegalArgumentException("Le participant avec l'ID spécifié n'existe pas.");
            }

            Participant participant = participantOptional.get();

            // Définir l'atelier et le participant dans la réservation
            reservation.setAtelier(atelier);
            reservation.setParticipant(participant);

            // Ajouter la réservation
            reservationService.addReservation(reservation);
        } catch (Exception e) {
            // Gestion des cas d'erreur lors de l'ajout de la réservation
            model.addAttribute("error", "Une erreur s'est produite lors de l'ajout de la réservation. Veuillez réessayer.");
            List<Participant> participants = participantService.getAllParticipants();
            model.addAttribute("participants", participants);
            return "ateliers/reservation";
        }

        // Si la réservation est ajoutée avec succès, rediriger vers la liste des réservations
        return "redirect:/reservations";
    }


    // Mettre à jour les informations d'une réservation
    @PostMapping("/reservations/{id}")
    public String updateReservation(@PathVariable("id") Long id, Reservation reservation) {
        reservation.setId(id);
        reservationService.updateReservation(reservation);
        return "redirect:/reservations"; // Redirection vers la liste des réservations
    }

    // Supprimer une réservation
    @PostMapping("/reservations/{id}/delete")
    public String deleteReservation(@PathVariable("id") Long id) {
        reservationService.deleteReservationById(id);
        return "redirect:/reservations"; // Redirection vers la liste des réservations
    }
}
