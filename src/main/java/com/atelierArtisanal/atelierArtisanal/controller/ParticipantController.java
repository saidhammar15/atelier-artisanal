package com.atelierArtisanal.atelierArtisanal.controller;

import com.atelierArtisanal.atelierArtisanal.Participant;
import com.atelierArtisanal.atelierArtisanal.service.ParticipantService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class ParticipantController {

    @Autowired
    private ParticipantService participantService;

    // Afficher la liste des participants
    @GetMapping("/participants")
    public String listParticipants(Model model) {
        List<Participant> participants = participantService.getAllParticipants();
        model.addAttribute("participants", participants);
        return "participants/listeParticipants"; // Vue Thymeleaf correspondante
    }

    // Afficher les détails d'un participant
    @GetMapping("/participants/{id}")
    public String showParticipantDetails(@PathVariable("id") Long id, Model model) {
        Participant participant = participantService.getParticipantById(id)
                .orElseThrow(() -> new IllegalArgumentException("Participant non trouvé avec l'identifiant : " + id));
        model.addAttribute("participant", participant);
        return "participants/detailsParticipant"; // Vue Thymeleaf correspondante
    }

    // Ajouter un nouveau participant
    @PostMapping("/participants")
    public String addParticipant(Participant participant) {
        participantService.addParticipant(participant);
        return "redirect:/participants"; // Redirection vers la liste des participants
    }

    // Mettre à jour les informations d'un participant
    @PostMapping("/participants/{id}")
    public String updateParticipant(@PathVariable("id") Long id, Participant participant) {
        participant.setId(id);
        participantService.updateParticipant(participant);
        return "redirect:/participants"; // Redirection vers la liste des participants
    }

    // Supprimer un participant
    @PostMapping("/participants/{id}/delete")
    public String deleteParticipant(@PathVariable("id") Long id) {
        participantService.deleteParticipantById(id);
        return "redirect:/participants"; // Redirection vers la liste des participants
    }
}
