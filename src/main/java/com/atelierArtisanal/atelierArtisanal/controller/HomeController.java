package com.atelierArtisanal.atelierArtisanal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String home(Model model) {

        // Renvoie le nom de la vue correspondante
        return "index"; // Assume que vous avez une vue appelée "index.html"
    }
    
}
