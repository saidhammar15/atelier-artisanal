package com.atelierArtisanal.atelierArtisanal.controller;

import com.atelierArtisanal.atelierArtisanal.Atelier;
import com.atelierArtisanal.atelierArtisanal.service.AtelierService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class AtelierController {

    @Autowired
    private AtelierService atelierService;

    // Afficher la liste des ateliers
    @GetMapping("/ateliers")
    public String listAteliers(Model model) {
        List<Atelier> ateliers = atelierService.getAllAteliers();
        model.addAttribute("ateliers", ateliers);
        return "ateliers/listeAteliers"; // Vue Thymeleaf correspondante
    }

    // Afficher les détails d'un atelier
    @GetMapping("/ateliers/{id}")
    public String showAtelierDetails(@PathVariable("id") Long id, Model model) {
        Atelier atelier = atelierService.getAtelierById(id)
                .orElseThrow(() -> new IllegalArgumentException("Atelier non trouvé avec l'identifiant : " + id));
        model.addAttribute("atelier", atelier);
        return "ateliers/detailsAtelier"; // Vue Thymeleaf correspondante
    }

    // Ajouter un nouvel atelier
    @PostMapping("/ateliers")
    public String addAtelier(Atelier atelier) {
        atelierService.addAtelier(atelier);
        return "redirect:/ateliers"; // Redirection vers la liste des ateliers
    }

    // Mettre à jour les informations d'un atelier
    @PostMapping("/ateliers/{id}")
    public String updateAtelier(@PathVariable("id") Long id, Atelier atelier) {
        atelier.setId(id);
        atelierService.updateAtelier(atelier);
        return "redirect:/ateliers"; // Redirection vers la liste des ateliers
    }

    // Supprimer un atelier
    @PostMapping("/ateliers/{id}/delete")
    public String deleteAtelier(@PathVariable("id") Long id) {
        atelierService.deleteAtelierById(id);
        return "redirect:/ateliers"; // Redirection vers la liste des ateliers
    }
}
