package com.atelierArtisanal.atelierArtisanal;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;


@Entity
public class Atelier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "artisan_id") // Mapping à la colonne artisan_id
    private Long artisanId;
    private String intitule; // Ajout de l'attribut intitule
    private String description; // Ajout de l'attribut description
    
    @ManyToOne
    @JoinColumn(name = "artisan_id", referencedColumnName = "id", insertable = false, updatable = false) // Correspondance entre les colonnes
    private Artisan artisan;
    
    @OneToMany(mappedBy = "atelier", cascade = CascadeType.ALL)
    private List<Reservation> reservations = new ArrayList<>();
    
    // Constructeurs
    public Atelier() {
    }
    
    public Atelier(long artisanId, String intitule, String description) {
        this.artisanId = artisanId;
        this.intitule = intitule;
        this.description = description;
    }
    
    // Getters et Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getArtisanId() {
        return artisanId;
    }

    public void setArtisanId(long artisanId) {
        this.artisanId = artisanId;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Artisan getArtisan() {
        return artisan;
    }

    public void setArtisan(Artisan artisan) {
        this.artisan = artisan;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    // Méthode pour ajouter une réservation à la liste
    public void addReservation(Reservation reservation) {
        this.reservations.add(reservation);
        reservation.setAtelier(this);
    }

    // Méthode pour supprimer une réservation de la liste
    public void removeReservation(Reservation reservation) {
        this.reservations.remove(reservation);
        reservation.setAtelier(null);
    }

    // Autres méthodes nécessaires selon les besoins de votre application
}
