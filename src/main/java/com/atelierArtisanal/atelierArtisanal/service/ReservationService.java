package com.atelierArtisanal.atelierArtisanal.service;

import com.atelierArtisanal.atelierArtisanal.Reservation;
import com.atelierArtisanal.atelierArtisanal.repository.ReservationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {
    
    @Autowired
    private ReservationRepository reservationRepository;
    
    // Retourne la liste de toutes les réservations
    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }
    
    // Retourne une réservation en fonction de son identifiant
    public Optional<Reservation> getReservationById(Long id) {
        return reservationRepository.findById(id);
    }
    
    // Ajoute une nouvelle réservation
    public Reservation addReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }
    
    // Met à jour les informations d'une réservation existante
    public Reservation updateReservation(Reservation reservation) {
        return reservationRepository.save(reservation);
    }
    
    // Supprime une réservation en fonction de son identifiant
    public void deleteReservationById(Long id) {
        reservationRepository.deleteById(id);
    }
    
    // Autres méthodes métier nécessaires selon les besoins de votre application
}
