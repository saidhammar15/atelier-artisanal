package com.atelierArtisanal.atelierArtisanal.service;

import com.atelierArtisanal.atelierArtisanal.Artisan;
import com.atelierArtisanal.atelierArtisanal.repository.ArtisanRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ArtisanService {
    
    @Autowired
    private ArtisanRepository artisanRepository;
    
    // Retourne la liste de tous les artisans
    public List<Artisan> getAllArtisans() {
        return artisanRepository.findAll();
    }
    
    // Retourne un artisan en fonction de son identifiant
    public Optional<Artisan> getArtisanById(Long id) {
        return artisanRepository.findById(id);
    }
    
    // Ajoute un nouvel artisan
    public Artisan addArtisan(Artisan artisan) {
        return artisanRepository.save(artisan);
    }
    
    // Met à jour les informations d'un artisan existant
    public Artisan updateArtisan(Artisan artisan) {
        return artisanRepository.save(artisan);
    }
    
    // Supprime un artisan en fonction de son identifiant
    public void deleteArtisanById(Long id) {
        artisanRepository.deleteById(id);
    }
    
    // Autres méthodes métier nécessaires selon les besoins de votre application
}
