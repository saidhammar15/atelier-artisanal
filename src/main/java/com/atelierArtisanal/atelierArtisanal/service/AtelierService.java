package com.atelierArtisanal.atelierArtisanal.service;

import com.atelierArtisanal.atelierArtisanal.Atelier;
import com.atelierArtisanal.atelierArtisanal.repository.AtelierRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AtelierService {
    
    @Autowired
    private AtelierRepository atelierRepository;
    
    // Retourne la liste de tous les ateliers
    public List<Atelier> getAllAteliers() {
        return atelierRepository.findAll();
    }
    
    // Retourne un atelier en fonction de son identifiant
    public Optional<Atelier> getAtelierById(Long id) {
        return atelierRepository.findById(id);
    }
    
    // Ajoute un nouvel atelier
    public Atelier addAtelier(Atelier atelier) {
        return atelierRepository.save(atelier);
    }
    
    // Met à jour les informations d'un atelier existant
    public Atelier updateAtelier(Atelier atelier) {
        return atelierRepository.save(atelier);
    }
    
    // Supprime un atelier en fonction de son identifiant
    public void deleteAtelierById(Long id) {
        atelierRepository.deleteById(id);
    }
    
    // Autres méthodes métier nécessaires selon les besoins de votre application
}
