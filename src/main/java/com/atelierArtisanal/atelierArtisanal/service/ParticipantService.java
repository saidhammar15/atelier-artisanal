package com.atelierArtisanal.atelierArtisanal.service;

import com.atelierArtisanal.atelierArtisanal.Participant;
import com.atelierArtisanal.atelierArtisanal.repository.ParticipantRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ParticipantService {
    
    @Autowired
    private ParticipantRepository participantRepository;
    
    // Retourne la liste de tous les participants
    public List<Participant> getAllParticipants() {
        return participantRepository.findAll();
    }
    
    // Retourne un participant en fonction de son identifiant
    public Optional<Participant> getParticipantById(Long id) {
        return participantRepository.findById(id);
    }
    
    // Ajoute un nouveau participant
    public Participant addParticipant(Participant participant) {
        return participantRepository.save(participant);
    }
    
    // Met à jour les informations d'un participant existant
    public Participant updateParticipant(Participant participant) {
        return participantRepository.save(participant);
    }
    
    // Supprime un participant en fonction de son identifiant
    public void deleteParticipantById(Long id) {
        participantRepository.deleteById(id);
    }
    
    // Autres méthodes métier nécessaires selon les besoins de votre application
}
