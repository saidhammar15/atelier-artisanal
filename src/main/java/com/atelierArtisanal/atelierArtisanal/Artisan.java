package com.atelierArtisanal.atelierArtisanal;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Artisan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String prenom;
    private String domaine; // Ajout de l'attribut domaine
    
    @OneToMany(mappedBy = "artisan", cascade = CascadeType.ALL)
    private List<Atelier> ateliers = new ArrayList<>();
    
    // Constructeurs
    public Artisan() {
    }
    
    public Artisan(String nom, String prenom, String domaine) {
        this.nom = nom;
        this.prenom = prenom;
        this.domaine = domaine;
    }
    
    // Getters et Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDomaine() {
        return domaine;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public List<Atelier> getAteliers() {
        return ateliers;
    }

    public void setAteliers(List<Atelier> ateliers) {
        this.ateliers = ateliers;
    }

    // Méthode pour ajouter un atelier à la liste
    public void addAtelier(Atelier atelier) {
        this.ateliers.add(atelier);
        atelier.setArtisan(this);
    }

    // Méthode pour supprimer un atelier de la liste
    public void removeAtelier(Atelier atelier) {
        this.ateliers.remove(atelier);
        atelier.setArtisan(null);
    }

    // Autres méthodes nécessaires selon les besoins de votre application
}
